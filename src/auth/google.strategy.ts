import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';
import { AuthService } from './auth.service';
import { Provider } from './provider.enum';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {

  constructor(
    private readonly authService: AuthService,
  ) {
    super({
      clientID    : '850626394267-qqi08qckv4tfuea057o90e08hbmp2n6l.apps.googleusercontent.com',     // <- Replace this with your client id
      clientSecret: '-P02XhSk_JKT3iZzDqErGW8O', // <- Replace this with your client secret
      callbackURL : 'http://localhost:3000/auth/google/callback',
      passReqToCallback: true,
      scope: ['profile'],
    });
  }

  async validate(request: any, accessToken: string, refreshToken: string, profile, done: Function) {
    try {
      console.log(profile);

      const jwt: string = await this.authService.validateOAuthLogin(profile.id, Provider.GOOGLE);
      const user = {
        jwt,
      };

      done(null, user);
    } catch (err) {
      // console.log(err)
      done(err, false);
    }
  }

}
